# Racket-UPI

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/racket-upi">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/racket-upi/">
    </a>
    <a href="https://gitlab.com/xgqt/racket-upi/pipelines">
        <img src="https://gitlab.com/xgqt/racket-upi/badges/master/pipeline.svg">
    </a>
</p>

**U**NIX tools inspired **P**rocedure **I**nterface for Racket.


## About

Racket library inspired by miscellaneous UNIX tools.

A lot of inspiration comes from the famous [BusyBox](https://busybox.net/).

Aim is to keep this library modules small but at the same time provide many of
them.
Any bigger tools like, for example, AWK or SH would probably be found
elsewhere.


## Installation

### Req

Use Req to install Racket-UPI from its project directory.

``` sh
raco req --everything --verbose
```

### Make

Use GNU Make to install Rall from its project directory.

```sh
make install
```


## Documentation

Documentation can be built locally by executing the command `make public`
or browsed online on either [GitLab pages](https://xgqt.gitlab.io/racket-upi/)
or [Racket-Lang Docs](https://docs.racket-lang.org/upi/).


## License

Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
