;; This file is part of racket-upi - Racket library inspired by UNIX tools.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-upi is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-upi is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-upi.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require (only-in racket/fixnum fixnum-for-every-system?))

(provide (all-defined-out))


(define-type Chmod-Who
  (U 'user 'group 'other 'all))

(define-type Chmod-How
  (U '+ '- '=))

(define-type Chmod-What
  (U 'read 'write 'execute 'all 'none))

(define-type Chmod-Mode
  (Vector Chmod-Who Chmod-How Chmod-What))


(define chmod-permissions-hash
  (hash 'all   (hash 'all     #o777
                     'read    #o444
                     'write   #o222
                     'execute #o111)
        'user  (hash 'all     #o700
                     'read    #o400
                     'write   #o200
                     'execute #o100)
        'group (hash 'all     #o070
                     'read    #o040
                     'write   #o020
                     'execute #o010)
        'other (hash 'all     #o007
                     'read    #o004
                     'write   #o002
                     'execute #o001)))


(define (permission-set? [path : Path-String]
                         [who  : Chmod-Who]
                         [what : Chmod-What])
        : Boolean
  (let* ([current-permissions
          (file-or-directory-permissions path 'bits)]
         [permission-mask
          (case who
            [(all)   #o777]
            [(user)  #o700]
            [(group) #o070]
            [else    ; other
             #o007])]
         [current-masked-permissions
          (bitwise-and current-permissions permission-mask)]
         [required-permissions
          (hash-ref (hash-ref chmod-permissions-hash who) what)])
    (= required-permissions
       (bitwise-and required-permissions
                    current-masked-permissions))))

(define (chmod [path : Path-String] [permission-mode : Chmod-Mode]) : Void
  (let* ([who  (vector-ref permission-mode 0)]
         [how  (vector-ref permission-mode 1)]
         [what (vector-ref permission-mode 2)]
         [current-permissions
          (file-or-directory-permissions path 'bits)]
         [permission-bits
          (case what
            [(none) #o000]
            [else
             (hash-ref (hash-ref chmod-permissions-hash who) what)])]
         [permission-result
          (case how
            [(+)
             (if (permission-set? path who what)
                 current-permissions
                 (+ current-permissions permission-bits))]
            [(-)
             (if (permission-set? path who what)
                 (- current-permissions permission-bits)
                 current-permissions)]
            [else  ; =
             (let ([permission-mask
                    (case who
                      [(all)   #o000]
                      [(user)  #o077]
                      [(group) #o707]
                      [else    ; other
                       #o770])])
               (+ (bitwise-and current-permissions permission-mask)
                  permission-bits))])])
    (cond
      [(and (equal? what 'all) (not (equal? how '=)))
       (error 'chmod
              "all permissions can not be added or subtracted, given ~v"
              permission-mode)]
      [(and (fixnum-for-every-system? permission-result)
            (>= permission-result 0))
       (file-or-directory-permissions path permission-result)]
      [else
       (error 'chmod "wrong permission-result, given ~v" permission-result)])))

(define (chmod* [path : Path-String] [permission-modes : (Listof Chmod-Mode)])
  (for ([permission-mode permission-modes])
    (chmod path permission-mode)))


#|
(define F "id.rkt")

(chmod F #(user  + execute))
(chmod F #(group = read))
(chmod F #(other - read))

(chmod F '(#(user  + execute) #(group = read) #(other - read)))
|#
