;; This file is part of racket-upi - Racket library inspired by UNIX tools.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-upi is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-upi is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-upi.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(provide with-environment-variables
         (rename-out [with-environment-variables env]))


(define-syntax with-environment-variables
  (syntax-rules ()
    [(_ ([environment-variable-name environment-variable-content] ...)
        body ...)
     (parameterize ([current-environment-variables
                     (environment-variables-copy
                      (current-environment-variables))])
       (environment-variables-set! (current-environment-variables)
                                   environment-variable-name
                                   environment-variable-content) ...
       body ...)]))
