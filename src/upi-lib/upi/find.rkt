;; This file is part of racket-upi - Racket library inspired by UNIX tools.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-upi is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-upi is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-upi.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require (only-in threading ~>>)
         "realpath.rkt")

;; NOTICE: Racket version 8.5: no typed definition of "file-or-directory-type"
(require/typed racket/base [file-or-directory-type (-> Path-String Symbol)])

(provide (all-defined-out))


;; TODO: GID, UID and that stuff.
;; TODO: check/limit fink follows.  #:follow-links? [follow-links? #f]


(define-type Find-Depth
  (Option Exact-Nonnegative-Integer))

(define-type Find-Type
  (Option Symbol))

(define-type Find-Exec
  (-> Path Any))


(define (find [where : Path-String (current-directory)]
              #:depth [depth : Find-Depth #false]
              #:name [name : Regexp #rx".*"]
              #:type [type : Find-Type #false]
              #:exec [exec : Find-Exec (lambda ([path : Path]) path)])
        : (Listof Any)
  (define real-where : Path
    (realpath where))
  (define (file-or-directory-type-equal? [path : Path]) : Boolean
    (if type
        (equal? (file-or-directory-type path) type)
        #true))
  (~>>
   (let loop : (Listof Path) ([root : Path-String real-where])
     (let ([root-depth
            : Integer
            (- (length (explode-path root))
               (length (explode-path real-where)))])
       (cond
         [(and depth (= root-depth depth))
          '()]
         [else
          (let ([listed-files
                 : (Listof Path)
                 (for/list ([path (directory-list root #:build? #true)]
                            #:when (regexp-match-exact? name path))
                   : (Listof Path)
                   path)])
            (append
             ;; Files
             (filter file-exists? listed-files)
             ;; Directories
             (for/lists ([lsts : (Listof (Listof Path))]
                         #:result (apply append lsts))
                        ([path : Path listed-files]
                         #:when (directory-exists? path))
               : (Listof Path)
               (append (list path)
                       (loop path)))))])))
   (filter file-or-directory-type-equal?)
   (map exec)))
