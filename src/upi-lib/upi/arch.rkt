;; This file is part of racket-upi - Racket library inspired by UNIX tools.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-upi is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-upi is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-upi.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

#|
NOTICE: Racket version 8.5: 'arch not available in Typed Racket
> Type Checker: No function domains matched in function application:
> ; Types: 'link  -> (U 'dll 'framework 'shared 'static)
> ;        'gc  -> (U '3m 'cgc)
> ;        'vm  -> Symbol
> ;        'os  -> (U 'macosx 'unix 'windows)
> ;          -> (U 'macosx 'unix 'windows)
> ; Arguments: 'arch
> ; Expected result: Symbol
> ;
> ;   in: (system-type (quote arch))
|#
(require/typed racket/base [system-type (-> 'arch Symbol)])

(provide arch)


(define (arch) : Symbol
  (system-type 'arch))
