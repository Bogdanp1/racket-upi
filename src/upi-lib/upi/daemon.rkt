;; This file is part of racket-upi - Racket library inspired by UNIX tools.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-upi is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-upi is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-upi.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(provide Daemon daemon)


(define-type Daemon
  (-> (Values (U Any Void) Daemon)))


(: daemon (-> (-> (U Any Void)) Daemon))

(define (daemon proc)
  (lambda ()
    (values (proc) (daemon proc))))


#|
Examples:
(define demon (daemon (lambda () 1)))  => Daemon
(demon)  => 1 & Daemon
(define-values (return demons-child) (demon))  => Daemon
(demons-child)  => 1 & Daemon
|#
