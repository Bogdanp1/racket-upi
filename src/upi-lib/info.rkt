#lang info


(define pkg-desc "Racket library inspired by UNIX tools. Core.")

(define version "0.3")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "typed-racket-lib"
    "typed-racket-more"
    "threading-lib"))
