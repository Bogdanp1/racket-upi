;; This file is part of racket-upi - Racket library inspired by UNIX tools.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-upi is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-upi is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-upi.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket upi/cp))


@title[#:tag "upi-cp"]{Cp}

@defmodule[upi/cp]

@defproc[
 (cp [source-path path-string?]
     [destination-path path-string?]
     [#:dereference? dereference? boolean? #false]
     [#:overwrite? overwrite? boolean? #false]
     [#:recurse? recurse? boolean? #false])
 void?
 ]{
}
