MAKE            ?= make
RACKET          := racket
RACO            := raco
SCRIBBLE        := $(RACO) scribble

DO-DOCS         := --no-docs
INSTALL-FLAGS   := --auto --skip-installed $(DO-DOCS)
REMOVE-FLAGS    := --force --no-trash $(DO-DOCS)
DEPS-FLAGS      := --check-pkg-deps --unused-pkg-deps
SETUP-FLAGS     := --tidy --avoid-main $(DO-DOCS) $(DEPS-FLAGS)
TEST-FLAGS      := --heartbeat --no-run-if-absent --submodule test --table

PWD             ?= $(shell pwd)

all: clean compile

clean-pkg-%:
	find $(*) -type d -name "compiled" -exec rm -dr {} +
compile-pkg-%:
	$(RACKET) -e "(require compiler/compiler setup/getinfo) (compile-directory-zos (path->complete-path \"$(*)\") (get-info/full \"$(*)/info.rkt\") #:skip-doc-sources? #t #:verbose #f)"
install-pkg-%:
	cd $(*) && $(RACO) pkg install $(INSTALL-FLAGS)
setup-pkg-%:
	$(RACO) setup $(SETUP-FLAGS) --pkgs $(*)
test-pkg-%:
	$(RACO) test $(TEST-FLAGS) --package $(*)
remove-pkg-%:
	$(RACO) pkg remove $(REMOVE-FLAGS) $(*)

clean-pkg-upi: clean-pkg-upi-lib clean-pkg-upi-doc clean-pkg-upi-test
clean-pkg-upi-doc: clean-pkg-upi-lib
clean-pkg-upi-test: clean-pkg-upi-lib

clean: clean-pkg-upi-lib clean-pkg-upi-doc clean-pkg-upi-test clean-pkg-upi

compile-pkg-upi: compile-pkg-upi-lib compile-pkg-upi-doc compile-pkg-upi-test
compile-pkg-upi-doc: compile-pkg-upi-lib
compile-pkg-upi-test: compile-pkg-upi-lib

compile: compile-pkg-upi-lib compile-pkg-upi-doc compile-pkg-upi-test compile-pkg-upi

install-pkg-upi: install-pkg-upi-lib install-pkg-upi-doc install-pkg-upi-test
install-pkg-upi-doc: install-pkg-upi-lib
install-pkg-upi-test: install-pkg-upi-lib

install: install-pkg-upi-lib install-pkg-upi-doc install-pkg-upi-test install-pkg-upi

setup-pkg-upi: setup-pkg-upi-lib setup-pkg-upi-doc setup-pkg-upi-test
setup-pkg-upi-doc: setup-pkg-upi-lib
setup-pkg-upi-test: setup-pkg-upi-lib

setup: setup-pkg-upi-lib setup-pkg-upi-doc setup-pkg-upi-test setup-pkg-upi

test-pkg-upi: test-pkg-upi-lib test-pkg-upi-doc test-pkg-upi-test
test-pkg-upi-doc: test-pkg-upi-lib
test-pkg-upi-test: test-pkg-upi-lib

test: test-pkg-upi-lib test-pkg-upi-doc test-pkg-upi-test test-pkg-upi

remove-pkg-upi-doc: remove-pkg-upi
remove-pkg-upi-lib: remove-pkg-upi remove-pkg-upi-doc remove-pkg-upi-test
remove-pkg-upi-test: remove-pkg-upi

remove: remove-pkg-upi remove-pkg-upi-test remove-pkg-upi-doc remove-pkg-upi-lib
